import ShoppingCartUpdate from "./edit-shopping-cart-class.js";
import SetItemsToBasket from "../Products/SetItemsToBasket.js";
import * as data from "../home.js";

// get products info from json and save it to localstorage
data.getData();

// declare variables to show shopping cart
const tableFill = document.querySelector(".table-fill table tbody");
const table = document.querySelector("table");
const template = document.querySelector("template");
const total = document.querySelector(".total");
const storageSC = JSON.parse(localStorage.getItem("Shopping Cart")) || [];

// show item(s) in the shopping cart
let a = 0;
function showTable() {
    for (let i = 0; i < storageSC.length; i++) {
        const clone = template.content.cloneNode(true);
        tableFill.appendChild(clone);

        const amount = document.querySelectorAll(".amount");
        const price2 = document.querySelectorAll(".price2");
        const picture = document.querySelectorAll(".pic");
        const nameSize = document.querySelectorAll(".name-size");

        // append all values to html
        amount[i].append(storageSC[i].amount);
        nameSize[i].append(storageSC[i].name + " " + storageSC[i].size);
        price2[i].append(storageSC[i].price * storageSC[i].amount + " €");
        picture[i].src = storageSC[i].image;
        a += parseInt(price2[i].textContent, 10);
        total.textContent = a + " €";
    }
}
showTable();

// declare variables to make an order
const notification = document.querySelector(".notification");
const notificationCount = document.getElementById("count-notification");
const payButton = document.querySelector(".btn-pay");
const totalPrice = document.querySelector(".total-price");
const afterPay = document.querySelector(".afterPay");

// after click on pay button make an order and save it to localstorage
payButton.addEventListener("click", () => {
    // hide shopping cart table
    if (notificationCount.textContent > 0) {
        table.style.display = "none";
        totalPrice.style.display = "none";

        // create elements and show confirmation after pay
        const confir = document.createElement("h1");
        confir.style.color = "lightcoral";
        confir.classList = "py-5";
        confir.textContent = "Thank you for shopping with us!";

        const divBtn = document.createElement("div");
        divBtn.classList = "py-5";

        const btnHome = document.createElement("a");
        btnHome.href = "../index.html";
        btnHome.classList.add("btn", "btn-secondary", "bg-gradient", "text-white", "rounded", "me-5", "mt-2");
        btnHome.textContent = "Back to home";

        const btnPr = document.createElement("a");
        btnPr.href = "../Products/products.html";
        btnPr.classList.add("btn", "btn-secondary", "bg-gradient", "text-white", "rounded", "mt-2");
        btnPr.textContent = "Back to products";

        afterPay.append(confir);
        afterPay.append(divBtn);
        divBtn.append(btnHome);
        divBtn.append(btnPr);

        // save order to local storage (see class)
        new SetItemsToBasket(storageSC).addOrder();

        // after order is made delete shopping card from localstorage and set notifications to zero
        localStorage.removeItem("Shopping Cart");
        notification.style.display = "none";
        notificationCount.textContent = 0;
    }
});


// declare variables to update shopping cart
const plusBtn = document.querySelectorAll(".plus");
const minusBtn = document.querySelectorAll(".minus");
const remove = document.querySelectorAll(".remove");
const tr = document.querySelectorAll("tbody tr");
const amount = document.querySelectorAll(".amount");
const price2 = document.querySelectorAll(".price2");

// let work plus, minus and delete to update shopping cart
for (let i = 0; i < storageSC.length; i++) {
    plusBtn[i].addEventListener("click", () => {
        // increase amount of product
        new ShoppingCartUpdate(amount[i], storageSC[i], storageSC, total).plusButton();

        // calculate prices
        new ShoppingCartUpdate(price2[i], 0, storageSC[i], 0).price2();

        // calculate notifications after click on plus
        new SetItemsToBasket(storageSC).notif();
    });

    minusBtn[i].addEventListener("click", () => {
        // decrease amount of product
        new ShoppingCartUpdate(amount[i], storageSC[i], storageSC, total).minusButton();

        // calculate prices
        new ShoppingCartUpdate(price2[i], 0, storageSC[i], 0).price2();

        // calculate notifications after click on minus
        new SetItemsToBasket(storageSC).notif();
    });

    remove[i].addEventListener("click", () => {
        // delete products from html and from localstorage
        new ShoppingCartUpdate(tr[i], total, 0, price2[i]).delete();
        new ShoppingCartUpdate(storageSC[i], storageSC).deleteFromStorage();
    });
}
