export default class ShoppingCartUpdate {
    constructor(amountHtml, numberLs, storage, priceTotal) {
        this.amount = amountHtml;
        this.number = numberLs;
        this.storage = storage;
        this.price = priceTotal;
    }
    plusButton() {
        // update shopping cart amount and prices after click on plus
        this.amount.textContent++;
        this.number.amount = Number(this.amount.textContent);
        this.price.textContent = parseInt(this.price.textContent, 10) + parseInt(this.number.price, 10) + " €";

        // save new value to localstorage
        localStorage.setItem("Shopping Cart", JSON.stringify(this.storage));
    }

    minusButton() {
        // update shopping cart amount and prices after click on minus
        if (this.amount.textContent > 1) {
            this.amount.textContent--;
            this.number.amount = Number(this.amount.textContent);

            this.price.textContent = parseInt(this.price.textContent, 10) - parseInt(this.number.price, 10) + " €";

            // save new value to localstorage
            localStorage.setItem("Shopping Cart", JSON.stringify(this.storage));
        }
    }

    price2() {
        // calculate total price of product
        this.amount.textContent = this.storage.price * this.storage.amount + " €";
    }

    delete() {
        // delete product from shopping cart and update prices
        this.amount.remove();
        this.number.textContent = parseInt(this.number.textContent, 10) - parseInt(this.price.textContent, 10) + " €";
    }

    deleteFromStorage() {
        // delete product from shopping cart in localstorage
        this.number = this.number.filter(item => item !== this.amount);
        localStorage.setItem("Shopping Cart", JSON.stringify(this.number));
        window.location.reload();
    }
}

