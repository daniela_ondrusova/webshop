export default class Update {
    constructor(reset, removeProduct, add) {
        this.reset = reset;
        this.remove = removeProduct;
        this.add = add;
    }
    addProd(id, name, size, price, img, amount) {
        // add new product to existing products and save it to localstorage
        this.add = {
            id: id,
            name: name,
            size: size,
            price: price,
            image: img,
            amount: amount,
        };

        let a = JSON.parse(localStorage.getItem("Products"));
        a.push(this.add);
        localStorage.setItem("Products", JSON.stringify(a));
    }

    editp(id, name, size, price, img, amount) {
        // edit existing product and save new value to localstorage
        if (this.reset.id == id) {
            this.reset = {
                id: this.reset.id = id,
                name: this.reset.name = name,
                size: this.reset.size = size,
                price: this.reset.price = price,
                image: this.reset.image = img,
                amount: this.reset.amount = amount,
            };
        }


        localStorage.setItem("Products", JSON.stringify(this.remove));
    }

    deleteFromStorage() {
        // delete product from html and from localstorage
        this.remove.remove();
        this.reset = this.reset.filter(item => item !== this.add);
        localStorage.setItem("Products", JSON.stringify(this.reset));
        window.location.reload();
    }
}