import Update from "../products-update.js";
import * as data from "../../home.js";

// get products info from json and save it to localstorage
data.getData();

// declare variables from input form
const id = document.querySelector(".id");
const name1 = document.querySelector(".name");
const size = document.querySelector(".size");
const price = document.querySelector(".price");
const amount = document.querySelector(".amount");
const img = document.querySelector(".img");
const view = document.getElementById("view");
const storageProducts = JSON.parse(localStorage.getItem("Products")) || [];

// take info from url as an input value
id.value = new URL(window.location.href).searchParams.get('id');
name1.value = new URL(window.location.href).searchParams.get('name');
size.value = new URL(window.location.href).searchParams.get('size');
price.value = new URL(window.location.href).searchParams.get('price');
img.value = new URL(window.location.href).searchParams.get('img');

// show img value in the input
view.src = img.value;
view.style.width = "50%";

// show image which will be uploaded
function viewImage() {
    view.src = img.value;
}
img.onchange = viewImage;

// check if products have the same id
const alertID = document.querySelector(".alert-id");

id.addEventListener("change", function () {
    alertID.textContent = "";
    alertID.style.color = "lightcoral";

    for (let i = 0; i < storageProducts.length; i++) {
        if (storageProducts[i].id == id.value) {
            alertID.textContent = "Choose another id.";
        }
    }
});


// edit product and save new values to localstorage
const btnEdit = document.querySelector(".btn-edit");
const heading = document.querySelector("h3");
const form = document.querySelector("form");

btnEdit.addEventListener("click", function () {
    const nam = name1.value;
    const siz = size.value;
    const pric = Number(price.value);
    const image = img.value;
    const am = Number(amount.value);

    // check if input is not empty
    if (nam != "" && siz != "" && pric != "" && image != "") {
        if (alertID.textContent != "Choose another id.") {
            for (let i = 0; i < storageProducts.length; i++) {
                new Update(storageProducts[i], storageProducts, 0).editp(id.value, nam, siz, pric, image, am);
            }

            heading.textContent = "Product changed!";
            form.style.display = "none";
            btnEdit.style.display = "none";
        }
    }
});