import Update from "../products-update.js";
import * as data from "../../home.js";

// get products info from json and save it to localstorage
data.getData();

// declare variables
const id = document.querySelector(".id");
const name1 = document.querySelector(".name");
const size = document.querySelector(".size");
const price = document.querySelector(".price");
const amount = document.querySelector(".amount");
const img = document.querySelector(".img");
const btnAdd = document.querySelector(".btn-add");
const storageProducts = JSON.parse(localStorage.getItem("Products")) || [];
const heading = document.querySelector("h3");
const form = document.querySelector("form");
const view = document.getElementById("view");
const alertID = document.querySelector(".alert-id");

// check if id already exist
id.addEventListener("change", function () {
    alertID.textContent = "";
    alertID.style.color = "lightcoral";

    for (let i = 0; i < storageProducts.length; i++) {
        if (storageProducts[i].id == id.value) {
            alertID.textContent = "Choose another id.";
        }
    }
});

// add new product to localstorage and html
btnAdd.addEventListener("click", function () {
    const pric = Number(price.value);
    const amnt = Number(amount.value);

    // check if input is not empty
    if (id.value != "" && name1.value != "" && size.value != "" && price.value != "" && img.value != "") {
        if (alertID.textContent != "Choose another id.") {
            new Update().addProd(id.value, name1.value, size.value, pric, img.value, amnt);

            heading.textContent = "Product added!";
            form.style.display = "none";
            btnAdd.style.display = "none";
        }
    }
});

// show image which will be uploaded
function viewImage() {
    view.src = img.value;
}
view.style.width = "50%";
img.onchange = viewImage;