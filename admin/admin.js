import Update from "./products-update.js";
import * as data from "../home.js";

// get products info from json and save it to localstorage
data.getData();

// declare variables to show orders
const storageOrders = JSON.parse(localStorage.getItem("Order")) || [];
const ordersTemplate = document.querySelector(".ordersTemplate");
const ordersTbody = document.querySelector(".ordersTbody");

function showOrders() {
    // show made orders
    for (let i = 0; i < storageOrders.length; i++) {
        const clone = ordersTemplate.content.cloneNode(true);
        ordersTbody.appendChild(clone);
        const orderNumber = document.querySelectorAll(".orderNumber");
        const price = document.querySelectorAll(".price");
        const datum = document.querySelectorAll(".datum");

        // append values to html
        orderNumber[i].append(i + 1);
        let sumPrice = 0;
        let date = 0;

        for (let j = 0; j < storageOrders[i].length; j++) {
            if (storageOrders[i][j].price != undefined) {
                sumPrice += storageOrders[i][j].price * storageOrders[i][j].amount;
            }
            date = storageOrders[i][j].date;
        }
        price[i].append(sumPrice + " €");
        datum[i].append(date);
    }
}
showOrders();

// declare variables to show products
const storageProducts = JSON.parse(localStorage.getItem("Products")) || [];
const productsTemplate = document.querySelector(".productsTemplate");
const productsTbody = document.querySelector(".productsTbody");

function showProducts() {
    // show products from loocalstorage
    for (let i = 0; i < storageProducts.length; i++) {
        const clone = productsTemplate.content.cloneNode(true);
        productsTbody.appendChild(clone);

        const picture = document.querySelectorAll(".picture");
        const orderNumberProducts = document.querySelectorAll(".orderNumberProducts");
        const nameSize = document.querySelectorAll(".name-size");
        const priceProducts = document.querySelectorAll(".priceProducts");

        // append values to html
        picture[i].src = storageProducts[i].image;
        orderNumberProducts[i].append(storageProducts[i].id);
        nameSize[i].append(storageProducts[i].name, " " + storageProducts[i].size);
        priceProducts[i].append(storageProducts[i].price + " €");
    }
}
showProducts();

// reset products to original json
const btnReset = document.querySelector(".reset");

function resetPr() {
    localStorage.removeItem("Products");
    data.getData();
    window.location.reload();
}
btnReset.onclick = resetPr;


// delete product from html and local storage after click
const del = document.querySelectorAll(".delete");
const tableTr = document.querySelectorAll(".productsTbody tr");

function deleteProduct() {
    for (let i = 0; i < storageProducts.length; i++) {
        del[i].addEventListener("click", () => {
            new Update(storageProducts, tableTr[i], storageProducts[i]).deleteFromStorage();
        });
    }
}
deleteProduct();

// show in url which product is editing
const edit = document.querySelectorAll(".edit");

for (let i = 0; i < storageProducts.length; i++) {
    edit[i].addEventListener("click", () => {
        const idP = storageProducts[i].id;
        const namP = storageProducts[i].name;
        const sizP = storageProducts[i].size;
        const prP = storageProducts[i].price;
        const imgP = storageProducts[i].image;
        const url = "./editProduct/edit-products.html";

        edit[i].href = `${url}?id=${idP}&name=${namP}&size=${sizP}&price=${prP}&img=${imgP}`;
    });
}


// admin info delete from localstorage after click on log out
const logOutBtn = document.querySelector(".log-out");

function logOut() {
    logOutBtn.addEventListener("click", () => {
        localStorage.removeItem("Admin");
    });
}
logOut();

// if admin info are not in localstorage then admin must be first logged in
function checkAdmin() {
    if (!localStorage.getItem("Admin")) {
        window.location.href = "../Login/login.html";
    }
}
checkAdmin();