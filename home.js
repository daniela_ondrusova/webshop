import SetItemsToBasket from "./Products/SetItemsToBasket.js";

// get products info from json and save it to localstorage
export function getData() {
    fetch("/products.json")
        .then(response => response.json())
        .then(data => {
            if (!localStorage.getItem("Products")) {
                localStorage.setItem("Products", JSON.stringify(data));
            }
        });
}
getData();

// show notifications from shopping cart
export function showNotif() {
    const notific = document.querySelector(".notification");
    const notificationCount = document.getElementById("count-notification");
    const shoppingCart = JSON.parse(localStorage.getItem("Shopping Cart")) || [];

    if (notific) {
        new SetItemsToBasket(shoppingCart).notif();
        if (notificationCount.textContent >= 1) {
            notific.classList.add("position-absolute", "top-10", "start-90", "translate-middle");
            notific.classList.add("badge", "rounded-pill", "bg-danger");
        }
    }
}

showNotif();

// if admin is logged in show it in html
export function ifAdminLogged() {
    const logIn = document.querySelector(".log-in");
    if (logIn) {
        new SetItemsToBasket(logIn).ifLoggedIn();
    }
}

ifAdminLogged();