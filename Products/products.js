import SetItemsToBasket from "./SetItemsToBasket.js";
import * as data from "../home.js";

// get products info from json and save it to localstorage
data.getData();

// declare variables
const content = document.querySelector(".content");
const template = document.querySelector("template");
const notific = document.querySelector(".notification");
const notificationCount = document.getElementById("count-notification");
const shoppingCart = JSON.parse(localStorage.getItem("Shopping Cart")) || [];
const storageProducts = JSON.parse(localStorage.getItem("Products")) || [];

// show info of each product from localstorage
function showContent() {
    for (let i = 0; i < storageProducts.length; i++) {
        const clone = template.content.cloneNode(true);
        content.appendChild(clone);

        const name = document.querySelectorAll(".name");
        const size = document.querySelectorAll(".size");
        const price = document.querySelectorAll(".price");
        const picture = document.querySelectorAll(".card-image");

        // append all values to html
        name[i].append(storageProducts[i].name);
        size[i].append(storageProducts[i].size);
        price[i].append(storageProducts[i].price + " €");
        picture[i].src = storageProducts[i].image;
    }
}
showContent();

// after click on buy save prosuct to localstorage - shopping cart
function buy() {
    const buyButton = document.querySelectorAll(".btn-buy");

    for (let i = 0; i < buyButton.length; i++) {
        buyButton[i].addEventListener("click", () => {
            new SetItemsToBasket(storageProducts[i], shoppingCart).addItemsToBasket();

            // update notifications
            new SetItemsToBasket(shoppingCart).notif();

            if (notificationCount.textContent >= 1) {
                notific.classList.add("position-absolute", "top-10", "start-90", "translate-middle");
                notific.classList.add("badge", "rounded-pill", "bg-danger");
            }

            // show product was added to the cart
            buyButton[i].textContent = "Added to the cart";
            buyButton[i].style.opacity = "0.75";
            setTimeout(function () {
                buyButton[i].textContent = "BUY";
                buyButton[i].style.opacity = "1";
            }, 1000);
        });
    }
}
buy();
