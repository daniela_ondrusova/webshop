export default class SetItemsToBasket {
    constructor(data, cart) {
        this.storage = cart;
        this.data = data;
        this.notificationCount = document.getElementById("count-notification");
    }

    addItemsToBasket() {
        // save shopping cart to localstorage
        if (!localStorage.getItem("Shopping Cart")) {
            localStorage.setItem("Shopping Cart", JSON.stringify(this.storage));
        }
        // change amount of product after click on buy
        let item = this.storage.find((x) => x.id === this.data.id);
        if (item === undefined) {
            this.storage.push(this.data);
        } else {
            item.amount += 1;
        }

        // save new value to localstorage
        localStorage.setItem("Shopping Cart", JSON.stringify(this.storage));
    }

    notif() {
        // sum amount of each product in shopping cart to show it in notification
        let a = 0;
        if (this.data != "") {
            for (let i = 0; i < this.data.length; i++) {
                a += this.data[i].amount;
            }
            this.notificationCount.textContent = Number(this.notificationCount.textContent);
            this.notificationCount.textContent = a;
        }
    }

    addOrder() {
        // after click on pay save order to localstorage
        if (!localStorage.getItem("Order")) {
            localStorage.setItem("Order", JSON.stringify([]));
        }

        // add date and time to the order and save it
        let a = JSON.parse(localStorage.getItem("Order"));
        this.data.push({ date: new Date().toLocaleString() });
        a.push(this.data);
        localStorage.setItem("Order", JSON.stringify(a));
    }

    ifLoggedIn() {
        // if admin is logged in show it in html
        if (localStorage.getItem("Admin")) {
            this.data.textContent = "Admin";
            this.data.href = "../admin/admin.html";
        }
    }
}

