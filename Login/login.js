import * as data from "../home.js";

// get products info from json and save it to localstorage
data.getData();

// get log in info from json
function getLogin() {
    fetch("./login-admin.json")
        .then(response => response.json())
        .then(dataLogin => {
            adminLogin(dataLogin);
        });
}

// declare variables to log in
const btnLogin = document.querySelector(".btn-login");
const nameInput = document.querySelector(".name-input");
const passwordInput = document.querySelector(".password-input");
const alertLogin = document.querySelector(".alert");

function adminLogin(dataLogin) {
    // check if log in info are correct
    btnLogin.addEventListener("click", (e) => {
        e.preventDefault();
        if (nameInput.value == dataLogin[0].name && passwordInput.value == dataLogin[0].password) {
            window.location.href = "../admin/admin.html";
            // save log in info to localstorage
            if (!localStorage.getItem("Admin")) {
                localStorage.setItem("Admin", JSON.stringify(dataLogin));
            }
        } else {
            alertLogin.textContent = "Username or Password is wrong!";
            alertLogin.style.color = "lightcoral";
        }
    });
}

getLogin();

// on input click hide alert if password was wrong
function inp() {
    alertLogin.textContent = "";
}
nameInput.onclick = inp;
passwordInput.onclick = inp;


