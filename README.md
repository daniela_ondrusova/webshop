# **Een webshop / bestelapplicatie**

Ik wil een normaal Web Trainee certificaat zonder studiepunten halen.<br>
<br>
## **Het project** 
- is een webshop met dames schoenen
- opent standaard in de eindgebruikersomgeving  
  - een gebruiker kan:    
    - alle producten zien
    - producten toevoegen aan de winkelwagen
    - zien of er producten in de winkelwagen zitten
    - producten uit winkelwagen verwijderen
    - bestellen (geen betalingen/persoonsgegevens)
    - contactgegevens zien<br>
    <br>                           
- heeft een admin-gedeelte in de webshop
  - de admin kan:
    - alle bestellingen zien
    - producten toevoegen / wijzigen / verwijderen
    - producten resetten naar de originele staat (JSON)<br>
    <br>
- er is gebruik gemaakt van  Bootstrap 5 framework <br>

### **Waar het project online te vinden is?**  
De webshop kunt u **[hier](https://fancy-webshop.netlify.app/)** vinden.<br>
<br>
Log in gegevens voor administrator:<br>
*Naam: admin*<br>
*Wachtwoord: admin@123*<br>

### **Instructies om project lokaal te runnen**
Om project lokaal te runnen moet u live-server installeren. Dat kan via npm of yarn.<br>
<br>
Live Server installeren met behulp van npm:<br>
```
npm install -g live-server  
```

Live Server installeren met behulp van yarn:<br>
```
yarn global add live-server  
```


Als live-server geïnstalleerd is ga naar de folder van uw project en open deze folder in je terminal.<br>
<br>
Schrijf `live-server` in je terminal. Het project is geopend in uw browser.

Om de live-server te stoppen druk `control + c`.<br>
<br>
Of via code editor bijvoordel Visual Studio Code met live-server extension.
